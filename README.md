# LegacyIP Warn

This project allows you to inject an warning banner for IPv4-users with NGINX used as a proxy.

It consists of two snippets and an example vhost configuration.

Blog Post: [https://margau.net/posts/2020-09-06-legacyip-warn/](https://margau.net/posts/2020-09-06-legacyip-warn/)
